# Coin Currency

Bitcoin, Ethereum, Binance Coin and Dogecoin Currency App with Vanilla JavaScript.


*Used API: https://min-api.cryptocompare.com*


# How to use another Currency ?

- Find your currency ticker
- Change `{newCurrencyTicker}` part
- Change `{wannaShowTicker}` part your `{newCurrencyTicker}` equal in market.


`let newCurrency = 'https://min-api.cryptocompare.com/data/price?fsym='+ {newCurrencyTicker} +'&tsyms={wannaShowTicker}'`


