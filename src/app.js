// API URL and URI

let BTC = document.querySelectorAll('p.btc-').text = 'BTC'
let ETH = document.querySelectorAll('p.eth-').text = 'ETH'
let BNB = document.querySelectorAll('p.bnb-').text = 'BNB'
let DOGE = document.querySelectorAll('p.doge-').text = 'DOGE'

let ApiBTC = 'https://min-api.cryptocompare.com/data/price?fsym='+ BTC +'&tsyms=BTC,USD,EUR,GBP'
let ApiETH = 'https://min-api.cryptocompare.com/data/price?fsym='+ ETH +'&tsyms=BTC,USD,EUR,GBP'
let ApiBNB = 'https://min-api.cryptocompare.com/data/price?fsym='+ BNB +'&tsyms=BTC,USD,EUR,GBP'
let ApiDOGE = 'https://min-api.cryptocompare.com/data/price?fsym='+ DOGE +'&tsyms=BTC,USD,EUR,GBP'

// Functions

async function getBTC(url) {
    const response = await fetch(url)
    const data = await response.json()
    document.querySelector('#btc-usd').innerHTML = "$ " + data.USD
    document.querySelector('#btc-gbp').innerHTML = "£ " + data.GBP
    document.querySelector('#btc-euro').innerHTML = "€ " + data.EUR
}

async function getETH(url) {
    const response = await fetch(url)
    const data = await response.json()
    document.querySelector('#eth-usd').innerHTML = "$ " + data.USD
    document.querySelector('#eth-gbp').innerHTML = "£ " + data.GBP
    document.querySelector('#eth-euro').innerHTML = "€ " + data.EUR}

async function getBNB(url) {
    const response = await fetch(url)
    const data = await response.json()
    document.querySelector('#bnb-usd').innerHTML = "$ " + data.USD
    document.querySelector('#bnb-gbp').innerHTML = "£ " + data.GBP
    document.querySelector('#bnb-euro').innerHTML = "€ " + data.EUR}

async function getDOGE(url) {
    const response = await fetch(url)
    const data = await response.json()
    document.querySelector('#doge-usd').innerHTML = "$ " + data.USD
    document.querySelector('#doge-gbp').innerHTML = "£ " + data.GBP
    document.querySelector('#doge-euro').innerHTML = "€ " + data.EUR}

// Call

getBTC(ApiBTC)
getETH(ApiETH)
getBNB(ApiBNB)
getDOGE(ApiDOGE)

// Refresh the data

setInterval( () => {
    getBTC(ApiBTC)
    getETH(ApiETH)
    getBNB(ApiBNB)
    getDOGE(ApiDOGE)
},   10000)

// Time

setInterval( () => {
    var time = new Date()
    document.querySelector('#time').innerHTML = time
}, 1000)
